#pretty colours
alias ls="ls --color -F"
alias grep='grep --color=auto'
alias ip='ip -color=auto'
##
alias removeorphans="sudo pacman -Rns $(pacman -Qtdq)"
alias zerotieron="systemctl start zerotier-one.service"
alias zerotieroff="systemctl stop zerotier-one.service"
alias zerotierrestart="systemctl restart zerotier-one.service"
alias bt="bluetoothctl"
alias nvimrc="nvim ~/.config/nvim/init.vim"
alias zshrc="nvim ~/.zshrc"
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

alias py="python"
alias woman="man"

##Work
alias werk="ssh sysadmin1@192.168.222.6"
