"My nvim config
"""""""""""""""
let g:mapleader = "\<Space>"
let g:maplocalleader = ','

set number	
set showmatch	
set visualbell
set hlsearch
set smartcase	
set incsearch
set mouse=a
set clipboard=unnamedplus
set autoindent	
set shiftwidth=2
set smartindent
set smarttab
set softtabstop=4	
set noshowmode
set cursorline
set ruler	
set encoding=utf8
set relativenumber
set undolevels=1000	
set backspace=indent,eol,start	
set noswapfile
set termguicolors
set timeoutlen=500
set ignorecase

syntax on
"Plugin configs
source $HOME/.config/nvim/plugins.vim
source $HOME/.config/nvim/plugins/coc.vim
source $HOME/.config/nvim/plugins/which-key.vim
source $HOME/.config/nvim/plugins/fzf.vim
source $HOME/.config/nvim/plugins/vimtex.vim

"vim-Which-key

"""Theme"""
colorscheme dracula
highlight Normal guibg=none
"let g:gruvbox_italic = '1'
""""""""""
lua require'colorizer'.setup()
lua require('lualine').setup{ options = {theme = 'dracula'}}


