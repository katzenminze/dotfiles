" Map leader to which_key
nnoremap <silent> <leader> :silent <c-u> :silent WhichKey '<Space>'<CR>
vnoremap <silent> <leader> :silent <c-u> :silent WhichKeyVisual '<Space>'<CR>

" Create map to add keys to
let g:which_key_map =  {}
" Define a separator
let g:which_key_sep = '→'
" set timeoutlen=100


" Not a fan of floating windows for this
let g:which_key_use_floating_win = 0

" Change the colors if you want
highlight default link WhichKey          Operator
highlight default link WhichKeySeperator DiffAdded
highlight default link WhichKeyGroup     Identifier
highlight default link WhichKeyDesc      Function

" Hide status line
"autocmd! FileType which_key
"autocmd  FileType which_key set laststatus=0 noshowmode noruler
"  \| autocmd BufLeave <buffer> set laststatus=2 noshowmode ruler


" Single mappings
let g:which_key_map['.'] = [ ':e $MYVIMRC'                , 'open init' ]
let g:which_key_map['='] = [ '<C-W>='                     , 'balance windows' ]
let g:which_key_map[','] = [ 'Startify'                   , 'start screen' ]
let g:which_key_map['d'] = [ ':bd'                        , 'delete buffer']
let g:which_key_map['h'] = [ '<C-W>s'                     , 'split below']
let g:which_key_map['T'] = [ ':Rg'                        , 'search text' ]
let g:which_key_map['v'] = [ '<C-W>v'                     , 'split right']

" Group mappings

" a is for actions
let g:which_key_map.a = {
      \ 'name' : '+actions' ,
      \ 'c' : [':ColorizerToggle'        , 'colorizer'],
      \ 'e' : [':CocCommand explorer'    , 'explorer'],
      \ 'n' : [':set nonumber!'          , 'line-numbers'],
      \ 'r' : [':set norelativenumber!'  , 'relative line nums'],
      \ 's' : [':let @/ = ""'            , 'remove search highlight'],
      \ }

" b is for buffer
let g:which_key_map.b = {
      \ 'name' : '+buffer' ,
      \ '1' : ['b1'        , 'buffer 1']        ,
      \ '2' : ['b2'        , 'buffer 2']        ,
      \ 'd' : ['bd'        , 'delete-buffer']   ,
      \ 'f' : ['bfirst'    , 'first-buffer']    ,
      \ 'h' : ['Startify'  , 'home-buffer']     ,
      \ 'l' : ['blast'     , 'last-buffer']     ,
      \ 'n' : ['bnext'     , 'next-buffer']     ,
      \ 'p' : ['bprevious' , 'previous-buffer'] ,
      \ '?' : ['Buffers'   , 'fzf-buffer']      ,
      \ }

" s is for search
let g:which_key_map.s = {
      \ 'name' : '+search' ,
      \ '/' : [':History/'              , 'history'],
      \ ';' : [':Commands'              , 'commands'],
      \ 'a' : [':Ag'                    , 'text Ag'],
      \ 'b' : [':BLines'                , 'current buffer'],
      \ 'B' : [':Buffers'               , 'open buffers'],
      \ 'c' : [':Commits'               , 'commits'],
      \ 'C' : [':BCommits'              , 'buffer commits'],
      \ 'f' : [':Files'                 , 'files'],
      \ 'g' : [':GFiles'                , 'git files'],
      \ 'G' : [':GFiles?'               , 'modified git files'],
      \ 'h' : [':History'               , 'file history'],
      \ 'H' : [':History:'              , 'command history'],
      \ 'l' : [':Lines'                 , 'lines'] ,
      \ 'm' : [':Marks'                 , 'marks'] ,
      \ 'M' : [':Maps'                  , 'normal maps'] ,
      \ 'p' : [':Helptags'              , 'help tags'] ,
      \ 'P' : [':Tags'                  , 'project tags'],
      \ 's' : [':CocList snippets'      , 'snippets'],
      \ 'S' : [':Colors'                , 'color schemes'],
      \ 't' : [':Rg'                    , 'text Rg'],
      \ 'T' : [':BTags'                 , 'buffer tags'],
      \ 'w' : [':Windows'               , 'search windows'],
      \ 'y' : [':Filetypes'             , 'file types'],
      \ 'z' : [':FZF'                   , 'FZF'],
      \ }
      " \ 's' : [':Snippets'     , 'snippets'],

"" g is for git
"let g:which_key_map.g = {
"      \ 'name' : '+git' ,
"      \ 'a' : [':Git add .'                        , 'add all'],
"      \ 'A' : [':Git add %'                        , 'add current'],
"      \ 'b' : [':Git blame'                        , 'blame'],
"      \ 'B' : [':GBrowse'                          , 'browse'],
"      \ 'c' : [':Git commit'                       , 'commit'],
"      \ 'd' : [':Git diff'                         , 'diff'],
"      \ 'D' : [':Gdiffsplit'                       , 'diff split'],
"      \ 'g' : [':GGrep'                            , 'git grep'],
"      \ 'G' : [':Gstatus'                          , 'status'],
"      \ 'h' : [':GitGutterLineHighlightsToggle'    , 'highlight hunks'],
"      \ 'H' : ['<Plug>(GitGutterPreviewHunk)'      , 'preview hunk'],
"      \ 'j' : ['<Plug>(GitGutterNextHunk)'         , 'next hunk'],
"      \ 'k' : ['<Plug>(GitGutterPrevHunk)'         , 'prev hunk'],
"      \ 'l' : [':Git log'                          , 'log'],
"      \ 'p' : [':Git push'                         , 'push'],
"      \ 'P' : [':Git pull'                         , 'pull'],
"      \ 'r' : [':GRemove'                          , 'remove'],
"      \ 's' : ['<Plug>(GitGutterStageHunk)'        , 'stage hunk'],
"      \ 'S' : [':!git status'                       , 'status'],
"      \ 't' : [':GitGutterSignsToggle'             , 'toggle signs'],
"      \ 'u' : ['<Plug>(GitGutterUndoHunk)'         , 'undo hunk'],
"      \ 'v' : [':GV'                               , 'view commits'],
"      \ 'V' : [':GV!'                              , 'view buffer commits'],
"      \ }
call which_key#register('<Space>', "g:which_key_map")

