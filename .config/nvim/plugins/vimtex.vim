filetype plugin indent on
let g:tex_flavor='latex'
let g:vimtex_quickfix_open_on_warning = 0
