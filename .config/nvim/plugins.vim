"Plugins
" Specify a directory for plugins
call plug#begin('~/.local/share/nvim/plugged')
"Colorizer
Plug 'norcalli/nvim-colorizer.lua'
"neovim-remote
Plug 'mhinz/neovim-remote'
"Autopairs
Plug 'jiangmiao/auto-pairs'
"CoC completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Startify
Plug 'mhinz/vim-startify'
"Vim-Which-key
Plug 'liuchengxu/vim-which-key'
"Emmet
Plug 'mattn/emmet-vim'
"Dracula
Plug 'dracula/vim', { 'as': 'dracula' } 
"Markdown
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
"FZF
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-rooter'
"Surroung
Plug 'tpope/vim-surround'
"Python
Plug 'Vimjas/vim-python-pep8-indent'
"lualine
Plug 'hoob3rt/lualine.nvim'
"Icons
Plug 'kyazdani42/nvim-web-devicons'
"vimtex
Plug 'lervag/vimtex'
"gtest
Plug 'alepez/vim-gtest'
call plug#end()
